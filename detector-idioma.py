import codecs
import math

def evaluate(freq_dictionary, lista_textos):
    result = [] 
    i = 1
    for text in lista_textos:
        detectedLanguage = "Unknown"
        min_error = 9999999999
        for lang in freq_dictionary:
            langLiteral = lang[0]
            dic = lang[1]
            gen_dic = dict()
            text_dic = generateDic(gen_dic, text)
            error = estimateError(dic, text_dic)
            if (error < min_error):
                detectedLanguage = langLiteral
                min_error = error
        result.append((i, detectedLanguage))
        i += 1
    return result
def estimateError(dic, data):
    error = 0
    for data_item in data:
        difference = 0
        if data_item in dic:
            difference = dic[data_item]
        error += math.sqrt(math.pow((data[data_item] - difference),2)) #se pasa a positiva la diferencia
    return error

def generateDic(dic, text):
    invalid_chars = '0123456789.:,;@"|¬°"#$%&/\()=¿^´`?¡!*[]{}_-+<>…~-“«'
    text_marks = ('\n', '\xad')
    char_count = 0
    for line in text:
        for char in line:
            if (char not in invalid_chars) and (char not in text_marks):
                char = char.lower()
                if char in dic:
                    dic[char] += 1
                else:
                    dic[char] = 1
                char_count += 1
    for term in dic:
        dic[term] /= char_count
    return dic

def train():
    eng_dic, fre_dic, ita_dic = dict(), dict(), dict()
    dictionary = (("French", fre_dic), ("English", eng_dic), ("Italian", ita_dic))
    for language in dictionary:
        try:
            lang = language[0]
            dic = language[1]
            fread = codecs.open("languageIdentificationData/training/" + str(lang), "r", "iso-8859-1")
            text = fread.read()
            dic = generateDic(dic, text)
            fread.close()
        except Exception as e:
            print(e)
            print("Error al procesar el archivo " + str(language))
    return dictionary

def getTexts():
    texts = list()
    try:
        fread = codecs.open("languageIdentificationData/test", "r", "iso-8859-1")
        texts = fread.readlines()
    except:
        print("Error al leer archivo test")
    return texts

def persist(result):
    try:
        fwrite = codecs.open("salida-detector-1.txt", "w", "iso-8859-1")
        for r in result:
            fwrite.write(str(r[0]) + " " + str(r[1]) + "\n")
        fwrite.close()
        print("Se genero el archivo salida-detector-1.txt con las respuestas")
    except:
        print("Error al persistir el archivo de salida")

def readSolutions():
    solutions = list()
    try:
        fread = codecs.open("languageIdentificationData/solution", "r", "iso-8859-1")
        text = fread.read().splitlines()
        for line in text:
            data = line.split()
            solutions.append((int(data[0]), data[1]))
        fread.close()
    except Exception as e:
        print(e)
    return solutions 

def calculateCorrects(result_list, solution_list):
    total = 0
    corrects = 0
    for r in result_list:
        if r in solution_list:
            corrects += 1
        total += 1
    return (corrects/total)*100

def calcularConUnigramas(lista_textos, solution_list):
    freq_dictionary = train()
    result_list = evaluate(freq_dictionary, lista_textos)
    correct_percent = calculateCorrects(result_list, solution_list)
    print("Idiomas detectados correctamente utilizando unigramas: " + str(correct_percent) + "%")

### Inicio de ejecucion ###

lista_textos = getTexts()
solution_list = readSolutions()
calcularConUnigramas(lista_textos, solution_list)

#descomentar para persistir archivo de salida con mismo formato que 'solution'
#persist(result_list)
